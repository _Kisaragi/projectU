//
// Created by @_Kisaragi on 10/5/21.
//

#ifndef FIRST_FIGUREPRINTER_H
#define FIRST_FIGUREPRINTER_H

#include <iostream>
#include <cmath>

class FigurePrinter{
protected:

    char character;

    void slopeLine(int position, int input, char mirror);

    void pyramidLine(int position, int input, char mirror);

    bool funcChecker(double x, double y);

    void funcLine(int position, int x, int y, int prec);

public:


    FigurePrinter(char x);

    void slope(int input, char mirror);

    void pyramid(int input, char mirror);

    void crystals(int input, int x, int y);

    void func(int x, int y, int prec);

    void setCharacter();
    char getCharacter();

 };




#endif //FIRST_FIGUREPRINTER_H
