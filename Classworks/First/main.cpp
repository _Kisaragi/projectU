#include <iostream>
#define PRINT(x, ch) for (int spi = 0; spi<x; spi++) std::cout << ch
#include "FigurePrinter.h"

int main() {
    FigurePrinter* printer = new FigurePrinter('#');
    int x, y, prec;
    std::cin >> x >> y >> prec;
    printer->func(x, y, prec);
}

