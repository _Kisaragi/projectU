//
// Created by @_Kisaragi on 10/5/21.
//

#include "FigurePrinter.h"

#define NEWLINE() std::cout << std::endl
#define REPEAT(character, times) for (int REPEAT_I = 0; REPEAT_I < times; ++REPEAT_I) std::cout << character

FigurePrinter::FigurePrinter(char x) {
    this->character=x;
}

void FigurePrinter::slopeLine(int position, int input, char mirror) {
    if (!(mirror&2)){
        if (!(mirror&1)) { // 0 |.
            REPEAT(this->character, position);
            REPEAT(' ', input-position);
        } else { // 1 .|
            REPEAT(' ', input-position);
            REPEAT(this->character, position);
        }
    } else {
        if (!(mirror&1)) { // 2 |`
            REPEAT(this->character, input-position);
            REPEAT(' ', position);
        } else { // 3 `|
            REPEAT(' ', position);
            REPEAT(this->character, (input-position));

        }
    }
}

void FigurePrinter::pyramidLine(int position, int input, char mirror) {
    slopeLine(position, input, mirror+1);
    if (input%2==1) std::cout << this->character;
    slopeLine(position, input, mirror);
}

// @todo: Сократить повторение кода в slope, pyramid и crystals

void FigurePrinter::slope(int input, char mirror) {
    for (int i = 1; i < input+1; ++i) {
        slopeLine(i, input, mirror);
        NEWLINE();
    }
}

void FigurePrinter::pyramid(int input, char mirror) {
    if (mirror&1) mirror = 2;
    else mirror = 0;
    for (int i = 0; i < (input)+1; ++i) {
        pyramidLine(i, input, mirror);
        NEWLINE();
    }
}

// @todo: Сократить месиво через pyramidLine();
void FigurePrinter::crystals(int input, int x, int y){
    int pyramidWidth = input/(2*y);
    bool mod2 = pyramidWidth%2;
    for (int k = 0; k < y; ++k) {
        for (int i = 0; i < (pyramidWidth)+1; ++i) {
            for (int j = 0; j < x; ++j) {
                slopeLine(i, pyramidWidth, 1);
                if (mod2) std::cout << this->character;
                slopeLine(i, pyramidWidth, 0);
            }
            NEWLINE();
            // @todo: Решить беду со значениями не кратными 2
        }
        for (int i = pyramidWidth; i >= 0; --i) {
            for (int j = 0; j < x; ++j) {
                slopeLine(i, pyramidWidth, 1);
                if (mod2) std::cout << this->character;
                slopeLine(i, pyramidWidth, 0);
            }
            NEWLINE();
        }
    }
}

bool FigurePrinter::funcChecker(double x, double y) {
    return ( (x*x+y*y-1)*(x*x+y*y-1)*(x*x+y*y-1)+x*x*y*y*y<=0 );
    //return (sin(x)-y<=0);
}

void FigurePrinter::funcLine(int position, int x, int y, int prec) {
    for(int i=-x/2;i<x/2;i++){
        if(funcChecker((double)i/prec, (double)(y/2-position)/prec)) std::cout << character;
        else std::cout << " ";
    }
}

void FigurePrinter::func(int x, int y, int prec) {
    for (int i = y/2; i >= -y/2; --i) {
        funcLine(i, x, y/2, prec);
        NEWLINE();
    }
}

/*
template<typename T1, typename T2>
void pyramid(T1 input, T2 mirror = 0){
    NEWLINE();
    for(int i = 1; i <= input; i=i+2) {
        for (int j = 1; j < (input-i)/2+1; ++j) std::cout << " ";
        for (int j = 1; j <=i; ++j) std::cout << "*";
        std::cout << std::endl;
    }
}

template<typename T1, typename T2, typename T3, typename T4>
void crystals(T1 input, T2 x = 1, T3 y = 1, T4 mirror = 0){
    NEWLINE();

}

*/

#undef NEWLINE
#undef REPEAT